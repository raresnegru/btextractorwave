from subprocess import call
import subprocess 

path_to_gdb="/home/yoctoadm/Documents/WaveSDK/sysroots/x86_64-elinasdk-linux/usr/bin/aarch64-elina-linux/aarch64-elina-linux-gdb"
path_to_bt="/home/yoctoadm/Documents/BTExtractorbase/"
processpath="/usr/bin/PathologyProc"
CDname="core_Pathology_P1722_S6_PathologyProc_LC63112_T313.gz"

def unzipCD():
	path_to_cd=path_to_bt+CDname
	call(["gunzip",path_to_cd])

def unzipDZ():
	path_to_DZ=path_to_bt+DZname
	call(["tar","-xvjf",path_to_DZ])

def callGDB():
	path_to_crashedapp=path_to_bt+"debugzip"+processpath
	path_to_cd=path_to_bt+"core_Pathology_P1722_S6_PathologyProc_LC63112_T313"
	command='''%s %s %s -ex "set sysroot /home/yoctoadm/Documents/BTExtractorbase/debugzip" -ex "bt" -ex "\n"'''%(path_to_gdb,path_to_crashedapp,path_to_cd)
	file=open("/home/yoctoadm/Documents/BTExtractorbase/logging.txt", "w")
	file.write(command)
	file.close()

	allbtdata=subprocess.check_output(command)
